﻿namespace P07.CustomHashFunction
{
    public class Person
    {
        public Person(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public override int GetHashCode()
        {
            return (FirstName.Length + Age) * (LastName.Length + Age) * Age;
        }

        public override bool Equals(object? obj)
        {
            Person otherPerson = obj as Person;
            bool areEqual = false;

            if (this.FirstName == otherPerson.FirstName && this.LastName == otherPerson.LastName && this.Age == otherPerson.Age)
            {
                areEqual = true;
            }

            return areEqual;
        }
    }
}
