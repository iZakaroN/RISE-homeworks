﻿namespace P04.KthSmallestElementBST
{
    public class BinarySearchTree
    {
        private static List<BinarySearchTree> result;

        public BinarySearchTree(int value, BinarySearchTree left, BinarySearchTree right)
        {
            Value = value;
            Left = left;
            Right = right;
            result = new List<BinarySearchTree>();
        }

        public int Value { get; set; }

        public BinarySearchTree Left { get; set; }

        public BinarySearchTree Right { get; set; }

        public static int GetKthSmallest(BinarySearchTree tree, int k)
        {
            List<BinarySearchTree> sortedElements = DfsInOrder(tree);
            int result = -1;

            if (k <= sortedElements.Count)
            {
                result = sortedElements[k - 1].Value;
            }

            return result;
        }

        private static List<BinarySearchTree> DfsInOrder(BinarySearchTree node)
        {
            if (node == null)
            {
                return result;
            }

            DfsInOrder(node.Left);
            result.Add(node);
            DfsInOrder(node.Right);

            return result;
        }
    }
}
