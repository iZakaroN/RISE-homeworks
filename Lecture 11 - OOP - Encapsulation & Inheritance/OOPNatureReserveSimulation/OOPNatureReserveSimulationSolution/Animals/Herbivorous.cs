﻿using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Herbivorous : Animal
    {
        private const int MAX_NUTRITION = 15;

        public Herbivorous(string name, int maxEnergy, IMap map)
            : base(name, maxEnergy, PlantFood.PlantFoodYoung, MAX_NUTRITION, map)
        {
        }

        protected override double StarvingCoefficient { get; init; } = 0.3;
        public override string NotHungryMessage { get; init; } = "I'm not hungry anymore. I need to run and play.";
        public override string HungryMessage { get; init; } = "I'm hungry...I need fresh green food.";
        public override string StarvingMessage { get; init; } = "I'm very hungry! I need food just now.";
        protected override int StartLifeSpan { get; init; } = 6;
        protected override int EndLifeSpan { get; init; } = 10;
        public override AnimalType Type { get; init; } = AnimalType.Herbivorous;
        public override AgeType AgeType { get; protected set; } = AgeType.Young;
        protected override HashSet<Food> Diet { get; set; } = PlantFood.PlantFoodYoung;
    }
}
