﻿namespace OOPNatureReserveSimulationSolution.Foods
{
    public class MeatFood
    {
        public static readonly HashSet<Food> MeatFoodYoung = new HashSet<Food>() {
            new Food("milk",4),
            new Food("insects",3),
            new Food("fresh meat",11) };

        public static readonly HashSet<Food> MeatFoodMiddle = new HashSet<Food>() {
            new Food("fresh meat",11),
            new Food("meat",12),
            new Food("bones",10),
            new Food("small animals",15),
            new Food("mouse", 15),
            new Food("deer", 15),
            new Food("eagle", 20),
            new Food("pig", 15)};

        public static readonly HashSet<Food> MeatFoodAdult = new HashSet<Food>() {
            new Food("fresh meat",11),
            new Food("meat",12),
            new Food("bones",10),
            new Food("small animals",15),
            new Food("big animals",15),
            new Food("mouse", 15),
            new Food("deer", 15),
            new Food("wolf", 20),
            new Food("eagle", 20),
            new Food("pig", 25)};
    }
}