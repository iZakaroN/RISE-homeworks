﻿using ToDoListWebApp.Models;

namespace ToDoListWebApp.Services
{
    public interface ITaskService
    {
        List<TaskViewModel> GetAllTasks();

        void AddNewTask(TaskFormModel task);

        bool EditTask(TaskViewModel taskModel);

        TaskViewModel GetById(int id);

        bool DeleteById(int id);
    }
}
