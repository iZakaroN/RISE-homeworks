﻿namespace FindMissingNumber2
{
    public class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class MissingNumber2
    {
        public static int FindMissing(int[] arr)
        {
            int n = arr.Length;

            if (n <= 1)
            {
                throw new ArgumentException("Input is not valid.");
            }

            //int minElement = arr.Min();
            //int maxElement = arr.Max();
            //int totalSum = (minElement + maxElement) * (n+1) / 2;
            //int actualSum = arr.Sum();

            int minElement = int.MaxValue;
            int maxElement = 0;
            int actualSum = 0;

            for (int i = 0; i < n; i++)
            {
                if (minElement > arr[i])
                {
                    minElement = arr[i];
                }

                if (maxElement < arr[i])
                {
                    maxElement = arr[i];
                }

                actualSum += arr[i];
            }

            int totalSum = (minElement + maxElement) * (n + 1) / 2;

            return totalSum - actualSum;
        }
    }
}