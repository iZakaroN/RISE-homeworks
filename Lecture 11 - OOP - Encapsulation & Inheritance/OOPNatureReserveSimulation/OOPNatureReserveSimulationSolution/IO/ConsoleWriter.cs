﻿namespace OOPNatureReserveSimulationSolution.IO
{
    public class ConsoleWriter : IWriter
    {
        public void WriteLine(object message)
        {
            Console.WriteLine(message);
        }
    }
}
