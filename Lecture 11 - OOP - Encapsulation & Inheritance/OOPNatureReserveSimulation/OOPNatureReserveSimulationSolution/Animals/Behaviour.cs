﻿using OOPNatureReserveSimulationSolution.Contracts;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Behaviour : IBehaviour
    {
        public string PartiallyEat(Animal animal, IEatable food)
        {
            return $"Animal {animal.Type} with name {animal.Name} partially ate {food.Name}.";
        }

        public string CompletelyEat(Animal animal, IEatable food)
        {
            return $"Animal {animal.Type} with name {animal.Name} completely  ate {food.Name}.";
        }

        public string NotHungry(Animal animal)
        {
            return $"Animal {animal.Type} with name {animal.Name}: {animal.NotHungryMessage}";
        }

        public string NotEat(Animal animal, IEatable food)
        {
            return $"Animal {animal.GetType().Name} with name {animal.Name} does not eat {food.Name}.";
        }

        public string Starve(Animal animal)
        {
            return $"Animal {animal.Type} with name {animal.Name}: {animal.StarvingMessage}";
        }

        public string Hungry(Animal animal)
        {
            return $"Animal {animal.Type} with name {animal.Name}: {animal.HungryMessage}";
        }

        public string Die(Animal animal)
        {
            return $"Animal {animal.Type} with name {animal.Name}: Help...I'm dying!";
        }
    }
}
