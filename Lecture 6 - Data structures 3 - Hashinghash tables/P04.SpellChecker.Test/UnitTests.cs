using NUnit.Framework;

namespace P04.SpellChecker.Test
{
    public class Tests
    {
        [Test]
        public void CheckReturnsManyWrongWords()
        {
            string text = "Today is sunny ar col";
            HashSet<string> wordList = new HashSet<string> { "today", "sunny", "is", "are", "tomorrow", "cold" };

            Assert.That(Checker.Check(wordList, text), Is.EqualTo(new string[] { "ar", "col" }));
        }

        [Test]
        public void CheckReturnsAllWordsWhenInputTextIsWrong()
        {
            string text = "Tuday s suny";
            HashSet<string> wordList = new HashSet<string> { "today", "sunny", "is", "are", "tomorrow", "cold" };

            Assert.That(Checker.Check(wordList, text), Is.EqualTo(new string[] { "tuday", "s", "suny" }));
        }

        [Test]
        public void CheckReturnsEmptyArrayWhenInputTextIsCorrect()
        {
            string text = "Today is sunny";
            HashSet<string> wordList = new HashSet<string> { "today", "sunny", "is", "are", "tomorrow", "cold" };

            Assert.That(Checker.Check(wordList, text), Is.EqualTo(new string[] { }));
        }
    }
}