﻿using Microsoft.AspNetCore.Mvc;
using ToDoListWebApp.Models;
using ToDoListWebApp.Services;

namespace ToDoListWebApp.Controllers
{
    public class TasksController : Controller
    {
        public const string GlobalMessageKey = "GlobalMessage";
        
        private readonly ITaskService taskService;

        public TasksController(ITaskService taskService)
        {
            this.taskService = taskService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<TaskViewModel> tasks = taskService.GetAllTasks();

            return View(tasks);
        }


        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Create(TaskFormModel task)
        {
            if (task.CreatedOn < DateTime.Now)
            {
                this.ModelState.AddModelError(String.Empty, "The date of task must be after the current date.");
            }

            if (!ModelState.IsValid)
            {
                return View(task);
            }

            taskService.AddNewTask(task);

            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            var task = taskService.GetById(id);

            if (task == null)
            {
                return BadRequest();
            }

            return View(task);
        }


        [HttpPost]
        public IActionResult Edit(TaskViewModel taskModel)
        {
            if (taskModel.CreatedOn < DateTime.Now)
            {
                this.ModelState.AddModelError(String.Empty, "The date of task must be after the current date.");
            }
<<<<<<< Updated upstream
            
            //if (!ModelState.IsValid)
            //{
            //    return View(taskModel);
            //}
=======
>>>>>>> Stashed changes

            //if (!ModelState.IsValid)
            //{
            //    return View(taskModel);
            //}

            var isEdited = taskService.EditTask(taskModel);

            if (!isEdited) 
            {
                return BadRequest();
            }

            return RedirectToAction("Details", new { id = taskModel.Id });
        }


        [HttpGet]
        public IActionResult Details(int id)
        {
            var task = taskService.GetById(id);

            if (task == null)
            {
                return BadRequest();
            }

            return View(task);
        }


        [HttpGet]
        public IActionResult Delete(int id)
        {
            var task = taskService.GetById(id);

            if (task == null)
            {
                return BadRequest();
            }

            return View(task);
        }

        [HttpPost]
        public IActionResult DeleteTask(int id)
        {
            bool isDeleted = taskService.DeleteById(id);

            if (!isDeleted)
            {
                return BadRequest();
            }

            this.TempData[GlobalMessageKey] = "Successfully deleted a task!";

            return RedirectToAction("Index");
        }
    }
}
