USE master

CREATE DATABASE ToDoList

USE ToDoList

CREATE TABLE Assignment(
Id INT PRIMARY KEY IDENTITY,
CreatedBy VARCHAR(100) NOT NULL,
AssignedTo VARCHAR(100) NOT NULL,
);

CREATE TABLE Task(
Id INT PRIMARY KEY IDENTITY,
Name VARCHAR(50) NOT NULL,
Description VARCHAR(1000) NOT NULL,
CreatedOn DATETIME2 NOT NULL,
AssignmentId INT FOREIGN KEY REFERENCES Assignment(Id) NOT NULL,
Done BIT NOT NULL DEFAULT 0
);


INSERT INTO Assignment
VALUES ('Ivan','Niki'),
		('Stefan','Georgi'),
		('Ani','Mimi');

INSERT INTO Task
VALUES ('Do your homework','Do your homework by tommorrow','2023-04-20',1,'false'),
		('Walk the dog','Walk the dog tonigh','2023-04-23',2,'false'),
		('Wash the dishes','Do not forget','2023-04-25',3,'false');