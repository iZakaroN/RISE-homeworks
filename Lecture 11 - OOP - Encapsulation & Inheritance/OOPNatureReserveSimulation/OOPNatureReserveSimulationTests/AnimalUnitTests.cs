using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationTests
{

    public class Tests
    {
        private Carnivorous wolf;
        private readonly IMap map;

        [SetUp]
        public void Setup()
        {
            wolf = new Carnivorous("wolf", 6, map);
        }

        [Test]
        public void AnimalCreatesYoungWithEnergyEqualToMaxEnergyAndNutritionEqualToMaxNutritionAndIsAlive()
        {
            Assert.That(wolf.MaxEnergy, Is.EqualTo(6));
            Assert.That(wolf.Energy, Is.EqualTo(6));
            Assert.That(wolf.IsAlive, Is.True);
            Assert.That(wolf.AgeType, Is.EqualTo(AgeType.Young));
            Assert.That(wolf.Nutrition, Is.EqualTo(20));
        }

        [Test]
        public void AnimalCanNotEatWhenHisEnergyIsEqualToMaxEnergyEvenFoodIsInDiet()
        {
            Food freshMeat = new Food("fresh meat", 11);
            wolf.Feed(freshMeat);

            Assert.That(wolf.MaxEnergy, Is.EqualTo(6));
            Assert.That(wolf.Energy, Is.EqualTo(5));
            Assert.That(wolf.IsAlive, Is.True);
            Assert.That(wolf.Nutrition, Is.EqualTo(20));
            Assert.That(freshMeat.Nutrition, Is.EqualTo(11));
        }

        [Test]
        public void AnimalDecreasesHisEnergyWhenFoodIsNotInDiet()
        {
            wolf.Feed(new Food("plant", 7));
            wolf.Feed(new Food("plant", 7));

            Assert.That(wolf.Energy, Is.EqualTo(4));
        }

        [Test]
        public void AnimalDecreasesHisEnergyAndIsNotAliveWhenFoodIsNotInDietAndIsStarvingTooLong()
        {
            wolf.Feed(new Food("plant", 7));
            wolf.Feed(new Food("plant", 7));
            wolf.Feed(new Food("plant", 7));
            wolf.Feed(new Food("plant", 7));
            wolf.Feed(new Food("plant", 7));
            wolf.Feed(new Food("plant", 7));

            Assert.That(wolf.Energy, Is.EqualTo(0));
            Assert.That(wolf.IsAlive, Is.False);
        }

        [Test]
        public void AnimalIncreasesHisEnergyWhenFoodIsInDietAndFoodLoseNutrition()
        {
            Food freshMeat = new Food("fresh meat", 11);
            Food plant = new Food("plant", 7);
            wolf.Feed(plant);
            wolf.Feed(plant);
            wolf.Feed(freshMeat);

            Assert.That(wolf.Energy, Is.EqualTo(6));
            Assert.That(wolf.IsAlive, Is.True);
            Assert.That(plant.Nutrition, Is.EqualTo(7));
            Assert.That(freshMeat.Nutrition, Is.EqualTo(9));
            Assert.That(freshMeat.IsAnimalFood(), Is.False);
        }

        [Test]
        public void AnimalToStringReturnsCorrectStringWhenAnimalIsCreated()
        {
            string actual = wolf.ToString();
            string expected = "Animal of Carnivorous type: name - wolf, age - Young, isAlive - True, energy - 6, life span - 0";

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void AnimalGrowsAndChangeHisDietWhenLifeSpanIncreases()
        {
            Food freshMeat = new Food("fresh meat", 11);
            Food plant = new Food("plant", 7);
            Animal mouse = new Herbivorous("mouse", 3, map);
            wolf.Feed(plant);
            wolf.Feed(plant);
            wolf.Feed(freshMeat);
            wolf.Feed(freshMeat);
            wolf.Feed(freshMeat);
            wolf.Feed(plant);
            wolf.Feed(mouse);
            wolf.Feed(mouse);
            wolf.Feed(mouse);

            Assert.That(wolf.AgeType, Is.EqualTo(AgeType.Middle));
            Assert.That(mouse.IsAlive, Is.False);
            Assert.That(mouse.Nutrition, Is.EqualTo(13));
            Assert.That(wolf.Energy, Is.EqualTo(6));
        }
    }
}