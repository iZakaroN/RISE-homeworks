﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoListWebApp.Data;
using ToDoListWebApp.Data.Models;
using ToDoListWebApp.Models;
using Task = ToDoListWebApp.Data.Models.Task;

namespace ToDoListWebApp.Services
{
    public class TaskService : ITaskService
    {
        private readonly ToDoListContext data;

        public TaskService(ToDoListContext data)
        {
            this.data = data;
        }

        [HttpGet]
        public List<TaskViewModel> GetAllTasks()
        {
            return this.data
                       .Tasks
                       .Select(t => new TaskViewModel()
                       {
                           Id = t.Id,
                           Name = t.Name,
                           CreatedOn = t.CreatedOn,
                           Done = t.Done,
                           Description = t.Description,
                           AssignmentId = t.AssignmentId,
                           Assignment = new AssignmentViewModel()
                           {
                               Id = t.AssignmentId,
                               CreatedBy = t.Assignment.CreatedBy,
                               AssignedTo = t.Assignment.AssignedTo,
                           }

                       }).ToList();
        }

        [HttpPost]
        public void AddNewTask(TaskFormModel task)
        {
            var newAssignment = new Assignment()
            {
                CreatedBy = task.CreatedBy,
                AssignedTo = task.AssignedTo
            };

            var newTask = new Task()
            {
                Name = task.Name,
                CreatedOn = task.CreatedOn,
                Description = task.Description,
                AssignmentId = newAssignment.Id,
                Assignment = newAssignment
            };

            data.Tasks.Add(newTask);
            data.SaveChanges();
        }

        [HttpPost]
        public bool EditTask(TaskViewModel taskModel)
        {
            Task? task = this.data
                             .Tasks
                             .Where(t => t.Id == taskModel.Id)
                             .Include(t => t.Assignment)
                             .FirstOrDefault();

            if (task == null)
            {
                return false;
            }

            task.Name = taskModel.Name;
            task.Description = taskModel.Description;
            task.CreatedOn = taskModel.CreatedOn;
            task.Done = taskModel.Done;
            task.AssignmentId = taskModel.AssignmentId;
            task.Assignment.CreatedBy = taskModel.Assignment.CreatedBy;
            task.Assignment.AssignedTo = taskModel.Assignment.AssignedTo;

            data.SaveChanges();

            return true;
        }

        [HttpGet]
        public TaskViewModel GetById(int id)
        {
            var task = this.data
                           .Tasks
                           .Where(t => t.Id == id)
                           .Include(t => t.Assignment)
                           .FirstOrDefault();

            if (task == null)
            {
                return null;
            }

            return new TaskViewModel()
            {
                Id = id,
                Name = task.Name,
                Description = task.Description,
                CreatedOn = task.CreatedOn,
                Done = task.Done,
                AssignmentId = task.Assignment.Id,
                Assignment = new AssignmentViewModel()
                {
                    Id = task.Assignment.Id,
                    CreatedBy = task.Assignment.CreatedBy,
                    AssignedTo = task.Assignment.AssignedTo
                }
            };
        }

        [HttpPost]
        public bool DeleteById(int id)
        {
            var taskToDelete = this.data
                                   .Tasks
                                   .FirstOrDefault(t => t.Id == id);
            var assignmentToDelete = this.data
                                         .Assignments
                                         .FirstOrDefault(a => a.Id == taskToDelete.AssignmentId);

            if (taskToDelete == null || assignmentToDelete == null)
            {
                return false;
            }

            data.Tasks.Remove(taskToDelete);
            data.Assignments.Remove(assignmentToDelete);

            data.SaveChanges();

            return true;
        }
    }
}
