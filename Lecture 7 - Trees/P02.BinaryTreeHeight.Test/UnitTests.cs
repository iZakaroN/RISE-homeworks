using NUnit.Framework;

namespace P02.BinaryTreeHeight.Test
{
    public class Tests
    {
        private BinaryTree binaryTree;

        [SetUp]
        public void Setup()
        {
            var node35 = new BinaryTree(35, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node76 = new BinaryTree(76, null, null);

            var node24 = new BinaryTree(24, null, null);
            var node38 = new BinaryTree(38, node35, null);
            var node69 = new BinaryTree(69, node61, node76);

            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);

            this.binaryTree = new BinaryTree(25, node11, node52);
        }

        [Test]
        public void TreeHeightReturnsCorrectResultWhenTreeHasSomeLevels()
        {
            var actual = BinaryTree.TreeHeight(binaryTree);

            Assert.That(actual, Is.EqualTo(3));
        }

        [Test]
        public void TreeHeightReturnsZeroForLeaf()
        {
            var actual = BinaryTree.TreeHeight(new BinaryTree(35, null, null));

            Assert.That(actual, Is.EqualTo(0));
        }
    }
}