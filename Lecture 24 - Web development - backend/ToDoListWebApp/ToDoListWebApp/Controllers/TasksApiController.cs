﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ToDoListWebApp.Models;
using ToDoListWebApp.Services;

namespace ToDoListWebApp.Controllers
{
    [Route("api/tasks")]
    [ApiController]
    public class TasksApiController : ControllerBase
    {
        private readonly ITaskService taskService;

        public TasksApiController(ITaskService taskService)
        {
            this.taskService = taskService;
        }

        [HttpGet("all")]
        public IActionResult GetAll()
        {
            List<TaskViewModel> tasks = taskService.GetAllTasks();

            return Ok(tasks);
        }

        [HttpGet("{id}")]
        public ActionResult<TaskViewModel> GetTask(int id)
        {
            var task = taskService.GetById(id);

            if (task == null)
            {
                return NotFound();
            }

            return task;
        }

        [HttpPost("create")]
        public async Task<ActionResult<TaskFormModel>> Create()
        {
            var requestBody = await GetRequestBody(Request);

            var inputTask = JsonConvert.DeserializeObject<InputTaskModel>(requestBody);

            if (inputTask == null)
            {
                return NotFound();
            }

            var task = new TaskFormModel()
            {
                Name = inputTask.Name,
                CreatedOn = DateTime.Parse(inputTask.CreatedOn),
                Description = inputTask.Description,
                CreatedBy = inputTask.CreatedBy,
                AssignedTo = inputTask.AssignedTo
            };

            taskService.AddNewTask(task);

            return CreatedAtAction("Create", task);
            //return Ok(task);
        }

        //[HttpPost("create")]
        ////public ActionResult<TaskFormModel> Create(InputTaskModel inputTask)
        //public IActionResult Create(InputTaskModel inputTask)
        //{
        //    var task = new TaskFormModel()
        //    {
        //        Name = inputTask.Name,
        //        CreatedOn = DateTime.Parse(inputTask.CreatedOn),
        //        Description = inputTask.Description,
        //        CreatedBy = inputTask.CreatedBy,
        //        AssignedTo = inputTask.AssignedTo
        //    };

        //    taskService.AddNewTask(task);

        //    //return CreatedAtAction("GetTask", task);
        //    return Ok(task);
        //}

        [HttpPut("edit/{id}")]
        public async Task<IActionResult> Edit()
        {
            var requestBody = await GetRequestBody(Request);

            var inputTask = JsonConvert.DeserializeObject<InputEditTaskModel>(requestBody);

            if (inputTask == null)
            {
                return NotFound();
            }

            var task = taskService.GetById(inputTask.Id);

            task.Name = inputTask.Name;
            task.Description = inputTask.Description;
            task.CreatedOn = DateTime.Parse(inputTask.CreatedOn);
            task.Assignment.CreatedBy = inputTask.CreatedBy;
            task.Assignment.AssignedTo = inputTask.AssignedTo;
            task.Done = inputTask.Done;

            var isEdited = taskService.EditTask(task);

            if (!isEdited)
            {
                return NotFound();
            }

            return NoContent();
        }

        //[HttpPut("edit/{id}")]
        //public IActionResult Edit(InputEditTaskModel inputTask)
        //{
        //    var task = taskService.GetById(inputTask.Id);

        //    task.Name = inputTask.Name;
        //    task.Description = inputTask.Description;
        //    task.CreatedOn = DateTime.Parse(inputTask.CreatedOn);
        //    task.Assignment.CreatedBy = inputTask.CreatedBy;
        //    task.Assignment.AssignedTo = inputTask.AssignedTo;
        //    task.Done = inputTask.Done;

        //    var isEdited = taskService.EditTask(task);

        //    if (!isEdited)
        //    {
        //        return BadRequest();
        //    }

        //    return NoContent();
        //}

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            bool isDeleted = taskService.DeleteById(id);

            if (!isDeleted)
            {
                return BadRequest();
            }

            return NoContent();
        }

        private async Task<string> GetRequestBody(HttpRequest request)
        {
            using StreamReader reader = new StreamReader(request.Body);
            string requestBody = await new StreamReader(request.Body).ReadToEndAsync();

            return requestBody;
        }
    }
}
