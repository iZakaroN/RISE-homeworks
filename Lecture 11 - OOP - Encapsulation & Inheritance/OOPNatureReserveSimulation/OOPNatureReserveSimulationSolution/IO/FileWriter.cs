﻿namespace OOPNatureReserveSimulationSolution.IO
{
    public class FileWriter : IWriter
    {
        public void WriteLine(object message)
        {
            File.AppendAllText("../../../output.txt", message + "\r\n");
        }
    }
}
