﻿namespace OOPNatureReserveSimulationSolution.Foods
{
    public static class PlantFood
    {
        public static readonly HashSet<Food> PlantFoodYoung = new HashSet<Food>() {
            new Food("grass", 2),
            new Food("leaves",3),
            new Food("seeds",4),
            new Food("fruits",5) };

        public static readonly HashSet<Food> PlantFoodMiddle = new HashSet<Food>() {
            new Food("leaves",3),
            new Food("plant",7),
            new Food("nuts",6),
            new Food("fruits",5) };

        public static readonly HashSet<Food> PlantFoodAdult = new HashSet<Food>() {
            new Food("leaves",3),
            new Food("fruits",5),
            new Food("big plants",8),
            new Food("bark",3) };
    }
}
