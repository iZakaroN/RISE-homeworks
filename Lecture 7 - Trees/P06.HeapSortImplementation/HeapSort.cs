﻿namespace P06.HeapSortImplementation
{
    public class HeapSort<T> where T : IComparable
    {
        private List<T> heap;

        public HeapSort()
        {
            heap = new List<T>();
        }

        public void Sort(T[] array)
        {
            int n = array.Length;

            for (int i = n / 2 - 1; i >= 0; i--)
            {
                Heapify(array, n, i);
            }

            for (int i = n - 1; i > 0; i--)
            {
                //Use tuple to swap elements
                (array[i], array[0]) = (array[0], array[i]);

                //T temp = array[0];
                //array[0] = array[i];
                //array[i] = temp;

                Heapify(array, i, 0);
            }
        }

        private void Heapify(T[] array, int size, int index)
        {
            int parentIndex = index;
            int leftChildIndex = 2 * index + 1;
            int rightChildIndex = 2 * index + 2;

            if (leftChildIndex < size && array[leftChildIndex].CompareTo(array[parentIndex]) > 0)
            {
                parentIndex = leftChildIndex;
            }

            if (rightChildIndex < size && array[rightChildIndex].CompareTo(array[parentIndex]) > 0)
            {
                parentIndex = rightChildIndex;
            }

            if (parentIndex != index)
            {
                //Use tuple to swap elements
                (array[parentIndex], array[index]) = (array[index], array[parentIndex]);

                //T temp = array[index];
                //array[index] = array[parentIndex];
                //array[parentIndex] = temp;

                Heapify(array, size, parentIndex);
            }
        }
    }
}
