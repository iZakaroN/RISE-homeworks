using NUnit.Framework;
using static P01.UniqueElements.Unique;

namespace P01.UniqueElements.Test
{
    public class Tests
    {
        [Test]
        public void GetUniqueWorksCorrect()
        {
            List<int> list = new List<int> { 1, 1, 2, 1, 2 };

            var actual = GetUnique(list);
            var expected = new List<int> { 1, 2 };

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}