﻿using OOPNatureReserveSimulationSolution.Contracts;
using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;
using System.Text;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public abstract class Animal : IEatable
    {
        private readonly IBehaviour behaviour;
        private IMap map;
        protected Animal(string name, int maxEnergy, HashSet<Food> diet, int maxNutrition, IMap map)
        {
            Name = name;
            Diet = diet;
            Energy = maxEnergy;
            MaxEnergy = maxEnergy;
            Nutrition = maxNutrition;
            MaxNutrition = maxNutrition;
            behaviour = new Behaviour();
            this.map = map;
        }
        protected virtual double StarvingCoefficient { get; init; } = 1.0;
        public virtual string NotHungryMessage { get; init; } = String.Empty;
        public virtual string HungryMessage { get; init; } = String.Empty;
        public virtual string StarvingMessage { get; init; } = String.Empty;
        protected virtual int StartLifeSpan { get; init; } = 0;
        protected virtual int EndLifeSpan { get; init; } = 0;
        public string Name { get; init; }
        public virtual AnimalType Type { get; init; } = AnimalType.None;
        public int Energy { get; protected set; }
        public int MaxEnergy { get; init; }
        protected abstract HashSet<Food> Diet { get; set; }
        public bool IsAlive => Energy > 0;
        public int LifeSpan { get; protected set; } = 0;
        public virtual AgeType AgeType { get; protected set; } = AgeType.None;
        public int Nutrition { get; set; }
        public int MaxNutrition { get; init; }

        public bool IsStarving => Energy < MaxEnergy * StarvingCoefficient;

        public bool IsAnimalFood()
        {
            this.Energy = 0;
            return true;
        }

        public Tile GetTile(IMap map)
        {
            return map.GetTile(this);
        }

        public virtual string Feed(IEatable food)
        {
            StringBuilder sb = new StringBuilder();

            if (IsAlive)
            {
                LifeSpan++;

                if (Diet.Any(f => f.Name == food.Name))
                {
                    int energyDifference = MaxEnergy - Energy;

                    ChangeNutritionMsg(food, energyDifference, sb);
                    ReduceNutrition(energyDifference);
                    ReduceAnimalFoodNutrition(food);
                }
                else
                {
                    ReduceEnergyMsg(food, sb);
                }

                Grow();
            }
            else
            {
                sb.AppendLine(behaviour.Die(this));
            }

            return sb.ToString().TrimEnd();
        }

        public int ReduceNutrition(int energyDifference)
        {
            int consumed = Nutrition;
            if (energyDifference < Nutrition)
            {
                consumed = energyDifference;
                Nutrition -= consumed;
            }
            else
            {
                Nutrition = 0;
            }

            return consumed;
        }
        protected virtual void Grow()
        {
            if (LifeSpan >= StartLifeSpan && LifeSpan < EndLifeSpan)
            {
                Diet = PlantFood.PlantFoodMiddle;
                AgeType = AgeType.Middle;
            }
            else if (LifeSpan >= EndLifeSpan)
            {
                Diet = PlantFood.PlantFoodMiddle;
                AgeType = AgeType.Adult;
            }
        }

        private string ChangeNutritionMsg(IEatable food, int energyDifference, StringBuilder sb)
        {
            if (energyDifference > 0 && energyDifference <= food.Nutrition)
            {
                Energy += food.ReduceNutrition(energyDifference);
                sb.AppendLine(behaviour.PartiallyEat(this, food));
            }
            else if (energyDifference > food.Nutrition)
            {
                Energy += food.ReduceNutrition(energyDifference);
                sb.AppendLine(behaviour.CompletelyEat(this, food));
            }
            else if (energyDifference == 0)
            {
                sb.AppendLine(behaviour.NotHungry(this));
                Energy--;
            }

            return sb.ToString().TrimEnd();
        }

        private string ReduceEnergyMsg(IEatable food, StringBuilder sb)
        {
            sb.AppendLine(behaviour.NotEat(this, food));

            if (IsStarving)
            {
                sb.AppendLine(behaviour.Starve(this));
                Console.Beep();
            }
            else
            {
                sb.AppendLine(behaviour.Hungry(this));
            }

            Energy--;

            return sb.ToString().TrimEnd();
        }

        private void ReduceAnimalFoodNutrition(IEatable food)
        {
            if (food.IsAnimalFood())
            {
            }
        }

        public override string ToString()
        {
            return $"Animal of {this.Type} type: name - {this.Name}, age - {this.AgeType}, isAlive - {this.IsAlive}, energy - {this.Energy}, life span - {this.LifeSpan}";
        }

        public string Move(StringBuilder sb)
        {
            Random random = new Random();
            int randomX = random.Next(this.map.Rows);
            int randomY = random.Next(this.map.Cols);
            Tile neighbourTile = map.GetTile(randomX, randomY);
            Tile animalTile = this.GetTile(map);

            sb.AppendLine($"Animal {this.Name} of {this.Type} type tries to move from {animalTile.Biome.Type}(capacity {animalTile.Biome.Capacity}) biome with coordinates ({animalTile.Coordinates.x},{animalTile.Coordinates.y}) to {neighbourTile.Biome.Type}(capacity {neighbourTile.Biome.Capacity}) biome with coordinates ({neighbourTile.Coordinates.x},{neighbourTile.Coordinates.y}).");

            if (this.IsStarving 
                && neighbourTile.Biome.Capacity > neighbourTile.Biome.Animals.Count 
                && animalTile.Coordinates != neighbourTile.Coordinates)
            {
                var tileFoods = neighbourTile.Biome.Foods.Select(f=> f.Name);
                var animalDiet = this.Diet.Select(f => f.Name);
                var mutualFoods = tileFoods.Intersect(animalDiet);
                if (mutualFoods.Any())
                {
                    animalTile.RemoveAnimal(this);
                    neighbourTile.AddAnimal(this);
                    sb.AppendLine($"Animal {this.Name} of {this.Type} type can move from {animalTile.Biome.Type} biome with coordinates ({animalTile.Coordinates.x},{animalTile.Coordinates.y}) to {neighbourTile.Biome.Type} biome with coordinates ({neighbourTile.Coordinates.x},{neighbourTile.Coordinates.y})");
                }
                else
                {
                    sb.AppendLine($"Animal {this.Name} of {this.Type} type can't move to {neighbourTile.Biome.Type} biome because there is no suitable food.");
                }
            }
            else
            {
                sb.AppendLine($"Animal {this.Name} of {this.Type} type can't move to {neighbourTile.Biome.Type} biome.");
            }

            return sb.ToString().TrimEnd();
        }
    }
}
