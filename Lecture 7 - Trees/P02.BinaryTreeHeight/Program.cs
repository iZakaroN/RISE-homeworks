﻿namespace P02.BinaryTreeHeight
{
    public class Program
    {
        static void Main(string[] args)
        {
            var node35 = new BinaryTree(35, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node76 = new BinaryTree(76, null, null);

            var node24 = new BinaryTree(24, null, null);
            var node38 = new BinaryTree(38, node35, null);
            var node69 = new BinaryTree(69, node61, node76);

            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var node25 = new BinaryTree(25, node11, node52);

            var output = BinaryTree.TreeHeight(node25);
            Console.WriteLine(output);
        }
    }
}