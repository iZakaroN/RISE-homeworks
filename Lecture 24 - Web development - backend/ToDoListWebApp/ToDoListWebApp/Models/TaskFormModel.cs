﻿using System.ComponentModel.DataAnnotations;

namespace ToDoListWebApp.Models
{
    public class TaskFormModel
    {
        [Required]
        [StringLength(50, MinimumLength = 2, ErrorMessage ="The name must be between 2 and 50 symbols.")]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 5, ErrorMessage = "The description must be between 2 and 50 symbols.")]
        public string Description { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "The name must be between 2 and 100 symbols.")]
        public string CreatedBy { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "The name must be between 2 and 100 symbols.")]
        public string AssignedTo { get; set; }
    }
}
