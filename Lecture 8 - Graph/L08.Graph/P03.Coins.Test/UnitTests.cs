using NUnit.Framework;

namespace P03.Coins.Test
{
    public class Tests
    {
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        public void CollectCoinsReturnsTrueWhenGraphIsCompletelyCyclic(int startNodeIndex)
        {
            Coin coin = new Coin(3);

            var actual = coin.CollectCoins(startNodeIndex, new int[,] { { 0, 1 }, { 1, 2 }, { 0, 2 } });

            Assert.That(actual, Is.True);
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void CollectCoinsReturnsTrueWhenGraphIsAcyclicAndPathExists(int startNodeIndex)
        {
            Coin coin = new Coin(5);

            var actual = coin.CollectCoins(startNodeIndex, new int[,] { { 0, 1 }, { 0, 2 }, { 2, 3 }, { 3, 4 } });

            Assert.That(actual, Is.True);
        }
    }
}