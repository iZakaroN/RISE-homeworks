﻿using OOPNatureReserveSimulationSolution.Contracts;

namespace OOPNatureReserveSimulationSolution.Foods
{
    public class Food : IEatable
    {
        public Food(string name, int maxNutrition)
        {
            Name = name;
            Nutrition = maxNutrition;
            MaxNutrition = maxNutrition;
        }

        public string Name { get; init; }
        public int Nutrition { get; set; }
        public int MaxNutrition { get; init; }

        public bool IsAnimalFood()
        {
            return false;
        }

        public int ReduceNutrition(int energyDifference)
        {
            int consumedNutrition;

            if (this.Nutrition < energyDifference)
            {
                consumedNutrition = this.Nutrition;
                this.Nutrition = 0;
            }
            else
            {
                this.Nutrition -= energyDifference;
                consumedNutrition = energyDifference;
            }

            return consumedNutrition;
        }
    }
}
