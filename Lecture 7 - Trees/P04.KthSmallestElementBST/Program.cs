﻿namespace P04.KthSmallestElementBST
{
    public class Program
    {
        static void Main(string[] args)
        {
            var node35 = new BinarySearchTree(35, null, null);
            var node61 = new BinarySearchTree(61, null, null);
            var node76 = new BinarySearchTree(76, null, null);

            var node24 = new BinarySearchTree(24, null, null);
            var node38 = new BinarySearchTree(38, node35, null);
            var node69 = new BinarySearchTree(69, node61, node76);

            var node11 = new BinarySearchTree(11, null, node24);
            var node52 = new BinarySearchTree(52, node38, node69);
            var node25 = new BinarySearchTree(25, node11, node52);

            var output = BinarySearchTree.GetKthSmallest(node25, 2);
            Console.WriteLine(output);
        }
    }
}