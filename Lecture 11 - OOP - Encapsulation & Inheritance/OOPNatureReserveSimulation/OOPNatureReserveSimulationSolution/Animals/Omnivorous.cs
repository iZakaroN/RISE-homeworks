﻿using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Omnivorous : Animal
    {
        private const int MAX_NUTRITION = 25;

        public Omnivorous(string name, int maxEnergy, IMap map)
            : base(name, maxEnergy, MixedFood.MixedFoodYoung, MAX_NUTRITION, map)
        {
        }

        public override string NotHungryMessage { get; init; } = "I'm not hungry anymore.";
        public override string HungryMessage { get; init; } = "I'm hungry...I want to eat, eat, eat...!";
        public override string StarvingMessage { get; init; } = "I'm starving. I'm so hungry, hungry, hungry...";
        protected override double StarvingCoefficient { get; init; } = 0.4;
        protected override int StartLifeSpan { get; init; } = 4;
        protected override int EndLifeSpan { get; init; } = 11;
        public override AnimalType Type { get; init; } = AnimalType.Omnivorous;
        public override AgeType AgeType { get; protected set; } = AgeType.Young;
        protected override HashSet<Food> Diet { get; set; } = MixedFood.MixedFoodYoung;

        protected override void Grow()
        {
            if (LifeSpan >= StartLifeSpan && LifeSpan < EndLifeSpan)
            {
                Diet = MixedFood.MixedFoodMiddle;
                AgeType = AgeType.Middle;
            }
            else if (LifeSpan >= EndLifeSpan)
            {
                Diet = MixedFood.MixedFoodAdult;
                AgeType = AgeType.Adult;
            }
        }
    }
}
