﻿using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Carnivorous : Animal
    {
        private const int MAX_NUTRITION = 20;

        public Carnivorous(string name, int maxEnergy, IMap map)
            : base(name, maxEnergy, MeatFood.MeatFoodYoung, MAX_NUTRITION, map)
        { }
        public override string NotHungryMessage { get; init; } = "I'm not hungry anymore. I'm so sleepy...";
        public override string HungryMessage { get; init; } = "I'm hungry...I need food just now!";
        public override string StarvingMessage { get; init; } = "I'm so hungry that I can eat an elephant!";
        protected override double StarvingCoefficient { get; init; } = 0.5;
        protected override int StartLifeSpan { get; init; } = 5;
        protected override int EndLifeSpan { get; init; } = 12;
        public override AnimalType Type { get; init; } = AnimalType.Carnivorous;
        public override AgeType AgeType { get; protected set; } = AgeType.Young;
        protected override HashSet<Food> Diet { get; set; } = MeatFood.MeatFoodYoung;

        protected override void Grow()
        {
            if (LifeSpan >= StartLifeSpan && LifeSpan < EndLifeSpan)
            {
                Diet = MeatFood.MeatFoodMiddle;
                AgeType = AgeType.Middle;
            }
            else if (LifeSpan >= EndLifeSpan)
            {
                Diet = MeatFood.MeatFoodAdult;
                AgeType = AgeType.Adult;
            }
        }
    }
}

