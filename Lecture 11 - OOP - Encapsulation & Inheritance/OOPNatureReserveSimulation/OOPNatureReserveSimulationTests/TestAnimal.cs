﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPNatureReserveSimulationTests
{
    public class TestAnimal : Animal
    {
        public TestAnimal(string name, 
            int maxEnergy, 
            HashSet<Food> diet, 
            int maxNutrition, 
            IMap map) 
            : base(name, maxEnergy, diet, maxNutrition, map)
        {
        }

        protected override HashSet<Food> Diet { get ; set; }
    }
}
