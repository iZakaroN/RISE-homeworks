﻿namespace P02.ArrayIntersection
{
    public class Intersection
    {
        public static int[] GetIntersection(int[] first, int[] second)
        {
            ////1-st decision
            //return  first.Intersect(second).ToArray();

            //2-nd decision
            Dictionary<int, int> numbersCount = new Dictionary<int, int>();
            int m = first.Length;
            int n = second.Length;

            HashSet<int> firstSet = new HashSet<int>(first);
            HashSet<int> secondSet = new HashSet<int>(second);

            CalculateNumbersCount(firstSet, numbersCount);
            CalculateNumbersCount(secondSet, numbersCount);

            int[] intersection = numbersCount.Where(x => x.Value > 1).Select(x => x.Key).ToArray();

            return intersection;
        }

        private static void CalculateNumbersCount(HashSet<int> set, Dictionary<int, int> numbersCount)
        {
            foreach (int number in set)
            {
                if (!numbersCount.ContainsKey(number))
                {
                    numbersCount[number] = 1;
                }
                else
                {
                    numbersCount[number]++;
                }
            }
        }
    }
}
