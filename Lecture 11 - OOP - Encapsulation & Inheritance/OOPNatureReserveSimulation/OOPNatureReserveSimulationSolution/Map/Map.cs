﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Bioms;
using OOPNatureReserveSimulationSolution.Contracts;

namespace OOPNatureReserveSimulationSolution.Map
{
    public class Map : IMap
    {
        private int rows;
        private int cols;
        private Tile[,] tiles;
        private readonly List<Biome> biomes;

        public Map(int rows, int cols)
        {
            this.rows = rows;
            this.cols = cols;
            this.tiles = new Tile[rows, cols];

            this.biomes = new List<Biome>()
            {
                new Forest(20, this),
                new Forest(10, this),
                new Savana(25, this),
                new Savana(15, this),
                new Plain(15, this)
            };

            Random random = new Random();
            GenerateMap(rows, cols, random);
        }

        private void GenerateMap(int rows, int cols, Random random)
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Biome randomBiome = biomes.ElementAt(random.Next(biomes.Count));
                    tiles[i, j] = new Tile(randomBiome,(i,j));
                }
            }
        }

        public int Rows => this.rows;
        public int Cols => this.cols;
        public Tile GetTile(int x, int y)
        {
            return tiles[x, y];
        }

        public Tile[,] GetTiles()
        {
            return tiles;
        }

        public List<Animal> GetAnimals()
        {
            return biomes.SelectMany(b => b.Animals).ToList();
        }

        public List<IEatable> GetFoods()
        {
            return biomes.SelectMany(b => b.Foods).ToList();
        }

        public Tile GetTile(Animal animal)
        {
            var tiles = this.GetTiles();
            foreach (var tile in tiles)
            {
                if (tile.Biome.Animals.Any(a => a.Name == animal.Name && a.MaxEnergy == animal.MaxEnergy))
                {
                    return tile;
                }
            }
            return null;
        }
    }
}
