﻿namespace OOPNatureReserveSimulationSolution.Enums
{
    public enum BiomType
    {
        None,
        Forest,
        Savana,
        Plain
    }
}
