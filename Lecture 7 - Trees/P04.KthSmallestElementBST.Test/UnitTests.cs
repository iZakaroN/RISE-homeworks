using NUnit.Framework;

namespace P04.KthSmallestElementBST.Test
{
    public class Tests
    {
        private BinarySearchTree binaryTree;


        [SetUp]
        public void Setup()
        {
            var node35 = new BinarySearchTree(35, null, null);
            var node61 = new BinarySearchTree(61, null, null);
            var node76 = new BinarySearchTree(76, null, null);

            var node24 = new BinarySearchTree(24, null, null);
            var node38 = new BinarySearchTree(38, node35, null);
            var node69 = new BinarySearchTree(69, node61, node76);

            var node11 = new BinarySearchTree(11, null, node24);
            var node52 = new BinarySearchTree(52, node38, node69);

            this.binaryTree = new BinarySearchTree(25, node11, node52);
        }

        [Test]
        public void GetKthSmallestReturnsTheSecondSmallestElement()
        {
            Assert.That(BinarySearchTree.GetKthSmallest(binaryTree,2), Is.EqualTo(24));
        }

        [Test]
        public void GetKthSmallestReturnsThe5thSmallestElement()
        {
            Assert.That(BinarySearchTree.GetKthSmallest(binaryTree, 5), Is.EqualTo(38));
        }

        [Test]
        public void GetKthSmallestReturns_1WhenKIsGreaterThanElementsCount()
        {
            Assert.That(BinarySearchTree.GetKthSmallest(binaryTree, 10), Is.EqualTo(-1));
        }
    }
}