﻿namespace _02.FormFields
{
    public class Graph
    {

        private int vertexCount;
        List<List<int>> matrix;
        public Graph(int count)
        {
            vertexCount = count * 2;
            matrix = new List<List<int>>();
            for (int i = 0; i < vertexCount; i++)
            {
                matrix.Add(new List<int>());
                for (int j = 0; j < vertexCount; j++)
                {
                    matrix[i].Add(0);
                }
            }
        }
        public void AddEdge(int start, int end)
        {
            matrix[start][end] = 1;
            matrix[end][start] = 1;
        }

        public bool DetectCycle()
        {
            bool[] visited = new bool[vertexCount];
            for (int i = 0; i < vertexCount; i++)
            {
                visited[i] = false;
            }

            for (int i = 0; i < vertexCount; i++)
            {
                if (!visited[i])
                {
                    if (IsCyclingDFS(i, visited, -1))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsCyclingDFS(int node, bool[] visited, int parent)
        {
            visited[node] = true;

            foreach (int i in matrix[node])
            {
                if (!visited[i])
                {
                    if (IsCyclingDFS(i, visited, node))
                    {
                        return true;
                    }
                }
                else if (i != parent)
                {
                    return false;
                }
            }

            return false;
        }
    }
}
