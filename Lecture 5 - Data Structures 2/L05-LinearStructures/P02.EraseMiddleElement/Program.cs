﻿namespace P02.EraseMiddleElement
{
    public class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class LinkedList
    {
        public static bool EraseMiddle(LinkedList<int> elements)
        {
            int size = elements.Count;

            if (size == 0)
            {
                return false;
            }

            if (elements is null)
            {
                throw new NullReferenceException();
            }

            int forRemove = elements.ElementAt(size / 2);
            elements.Remove(forRemove);

            return true;
        }
    }
}