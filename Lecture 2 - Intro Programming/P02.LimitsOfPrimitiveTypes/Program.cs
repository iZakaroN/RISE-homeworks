﻿namespace P02.LimitsOfPrimitiveTypes
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Int minValue: {int.MinValue}");
            int intMaxValue = int.MaxValue;
            Console.WriteLine($"Int maxValue: {intMaxValue}");
            Console.WriteLine($"Int maxValue + 1: {intMaxValue + 1}");

            Console.WriteLine($"Double minValue: {double.MinValue}");
            double doubleMaxValue = double.MaxValue;
            Console.WriteLine($"Double maxValue: {doubleMaxValue}");
            Console.WriteLine($"Double maxValue + 1: {doubleMaxValue + 1}");

            Console.WriteLine($"Char minValue: {char.MinValue}");
            char charMaxValue = char.MaxValue;
            Console.WriteLine($"Char maxValue: {charMaxValue}");
            Console.WriteLine($"Char maxValue + 1: {charMaxValue + 1}");

            Console.WriteLine($"Short minValue: {short.MinValue}");
            short shortMaxValue = short.MaxValue;
            Console.WriteLine($"Short maxValue: {shortMaxValue}");
            Console.WriteLine($"Short maxValue + 1: {shortMaxValue + 1}");
            Console.WriteLine($"Short maxValue + 1 as short: {(short)(shortMaxValue + 1)}");

            Console.WriteLine($"Long minValue: {long.MinValue}");
            long longMaxValue = long.MaxValue;
            Console.WriteLine($"Long maxValue: {longMaxValue}");
            Console.WriteLine($"Long maxValue + 1: {longMaxValue + 1}");
        }
    }
}