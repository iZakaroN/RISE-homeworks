using NUnit.Framework;

namespace P03.NonRepeatingCharacters.Test
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetNonRepeatingCharsReturnsCorrectChars()
        {
            string input = "aabaccdf";

            Assert.That(Character.GetNonRepeatingChars(input), Is.EqualTo(new char[] { 'b', 'd', 'f' }));
        }

        [Test]
        public void GetNonRepeatingCharsReturnsEmptyArrayWhenAllCharsAreRepeating()
        {
            string input = "aabaccb";

            Assert.That(Character.GetNonRepeatingChars(input), Is.EqualTo(new char[] { }));
        }

        [Test]
        public void GetFirstUniqueCharacterReturns_1WhenAllCharsAreRepeating()
        {
            string input = "aabaccb";

            Assert.That(Character.GetFirstUniqueCharacter(input), Is.EqualTo(-1));
        }

        [Test]
        public void GetFirstUniqueCharacterReturnsCorrectIndex()
        {
            string input = "aabaccdf";

            Assert.That(Character.GetFirstUniqueCharacter(input), Is.EqualTo(2));
        }
    }
}