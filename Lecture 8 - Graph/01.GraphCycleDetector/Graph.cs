﻿namespace _01.GraphCycleDetector
{
    public class Graph
    {
        private int vertexCount;
        List<List<int>> matrix;
        public Graph(int count)
        {
            vertexCount = count;
            matrix = new List<List<int>>();
            for (int i = 0; i < vertexCount; i++)
            {
                matrix.Add(new List<int>());
                for (int j = 0; j < vertexCount; j++)
                {
                    matrix[i].Add(0);
                }
            }
        }
        public void AddEdge(int start, int end)
        {
            matrix[start][end] = 1;
            matrix[end][start] = 1;
        }

        public bool DetectCycle()
        {
            HashSet<int> visited = new HashSet<int>();

            for (int i = 0; i < vertexCount; i++)
            {
                if (!visited.Contains(i))
                {
                    if (IsCyclingDFS(i, visited, default))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsCyclingDFS(int node, HashSet<int> visited, int parent)
        {
            visited.Add(node);

            foreach (int i in matrix[node])
            {
                if (!visited.Contains(i))
                {
                    if (IsCyclingDFS(i, visited, node))
                    {
                        return true;
                    }
                }
                else if (i != parent)
                {
                    return false;
                }
            }

            return false;
        }

        //public void DFSIterative(int start)
        //{
        //    Stack<int> orderedNodes = new Stack<int>();
        //    HashSet<int> visited = new HashSet<int>();
        //    orderedNodes.Push(start);

        //    while (orderedNodes.Count > 0)
        //    {
        //        var current = orderedNodes.Pop();
        //        visited.Add(current);
        //        Console.WriteLine(current);

        //        for (int i = 0; i < vertexCount; i++)
        //        {
        //            if (matrix[current][i] == 1 && !visited.Contains(i))
        //            {
        //                orderedNodes.Push(i);
        //            }
        //        }
        //    }
        //}

        //public void DFSRecursive(int node, HashSet<int> visited)
        //{
        //    visited.Add(node);

        //    for (int i = 0; i < vertexCount; i++)
        //    {
        //        if (matrix[node][i] == 1 && !visited.Contains(i))
        //        {
        //            DFSRecursive(i, visited);
        //        }
        //    }
        //    Console.WriteLine(node);
        //}
    }
}
