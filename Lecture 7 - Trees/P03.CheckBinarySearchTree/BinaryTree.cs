﻿namespace P03.CheckBinarySearchTree
{
    public class BinaryTree
    {
        private static List<BinaryTree> result;

        public BinaryTree(int value, BinaryTree left, BinaryTree right)
        {
            Value = value;
            Left = left;
            Right = right;
            result = new List<BinaryTree>();
        }

        public int Value { get; set; }

        public BinaryTree Left { get; set; }

        public BinaryTree Right { get; set; }

        //Second decision
        public static bool CheckBST2(BinaryTree node)
        {
            bool isBST = false;

            List<BinaryTree> inOrderElements = DfsInOrder(node);
            List<BinaryTree> sortedElements = inOrderElements
                .OrderBy(n => n.Value)
                .Select(n => n)
                .ToList();

            if (inOrderElements.SequenceEqual(sortedElements))
            {
                isBST = true;
            }

            return isBST;
        }

        public static List<BinaryTree> DfsInOrder(BinaryTree node)
        {
            if (node == null)
            {
                return result;
            }

            DfsInOrder(node.Left);
            result.Add(node);
            DfsInOrder(node.Right);

            return result;
        }


        //First decision
        public static bool CheckBST(BinaryTree node)
        {
            bool isBST = false;

            if (node == null)
            {
                isBST = true;
            }

            if (node.Left != null && GetMaxValue(node.Left) > node.Value)
            {
                isBST = false;
            }

            if (node.Right != null && GetMinValue(node.Right) < node.Value)
            {
                isBST = false;
            }

            if (CheckBST(node.Left) == false || CheckBST(node.Right) == false)
            {
                isBST = false;
            }

            return isBST;
        }

        private static int GetMaxValue(BinaryTree node)
        {
            if (node == null)
            {
                return int.MinValue;
            }

            int leftMax = GetMaxValue(node.Left);
            int rightMax = GetMaxValue(node.Right);

            return Math.Max(node.Value, Math.Max(leftMax, rightMax));
        }

        private static int GetMinValue(BinaryTree node)
        {
            if (node == null)
            {
                return int.MaxValue;
            }

            int leftMax = GetMinValue(node.Left);
            int rightMax = GetMinValue(node.Right);

            return Math.Min(node.Value, Math.Min(leftMax, rightMax));
        }
    }
}
