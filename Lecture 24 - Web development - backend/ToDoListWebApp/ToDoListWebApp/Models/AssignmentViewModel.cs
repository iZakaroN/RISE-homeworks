﻿using System.ComponentModel.DataAnnotations;

namespace ToDoListWebApp.Models
{
    public class AssignmentViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "The name must be between 2 and 100 symbols.")]
        public string CreatedBy { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "The name must be between 2 and 100 symbols.")]
        public string AssignedTo { get; set; }

        public virtual List<TaskViewModel> Tasks { get; set; }
    }
}
