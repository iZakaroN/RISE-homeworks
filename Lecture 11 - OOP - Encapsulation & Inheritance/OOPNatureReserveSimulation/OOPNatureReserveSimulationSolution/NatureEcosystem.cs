﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Contracts;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.IO;
using OOPNatureReserveSimulationSolution.Map;
using System.Text;

namespace OOPNatureReserveSimulationSolution
{
    public class NatureEcosystem
    {
        private readonly IWriter writer;
        private IMap map;
        private List<Animal> allAnimals;
        private List<IEatable> allFoods;

        public NatureEcosystem(IWriter writer, IMap map)
        {
            this.writer = writer;
            this.map = map;
            allAnimals = map.GetAnimals();
            allFoods = map.GetFoods();
        }

        private List<Animal> GetAnimals() => allAnimals;
        private List<IEatable> GetFoods() => allFoods;

        public void RunSimulation(bool getDetailedStatistics, int turns = 0)
        {
            List<Animal> animals = GetAnimals();
            List<IEatable> foods = GetFoods();
            List<Animal> aliveAnimals = animals.Where(a => a.IsAlive).ToList();

            if (getDetailedStatistics)
            {
                while (aliveAnimals.Any(a => a.IsAlive))
                {
                    string feedMsg = FeedAnimals(animals);
                    writer.WriteLine(feedMsg);
                    RegenerateFood(foods);
                    string output = GetStatistics(animals);
                    writer.WriteLine(output);
                    writer.WriteLine("");
                }
            }
            else
            {
                for (int i = 1; i <= turns; i++)
                {
                    FeedAnimals(animals);
                    RegenerateFood(foods);
                    string output = GetSummary(animals, i);
                    writer.WriteLine(output);
                    writer.WriteLine("");
                }
            }
        }

        public void LiveSimulation(bool getDetailedStatistics, int turns = 0)
        {
            StringBuilder sb = new StringBuilder();
            string startingMap = PrintStartingMap(sb);
            writer.WriteLine("Initial map:");
            writer.WriteLine(startingMap);
            writer.WriteLine("");

            List<Animal> aliveAnimals = map.GetAnimals().Where(a => a.IsAlive).ToList();
            List<IEatable> foods = GetFoods();
            Random random = new Random();

            //while (aliveAnimals.Any())
            //{
            //    Animal randomAnimal = aliveAnimals.ElementAt(random.Next(aliveAnimals.Count));
            //    Tile currentTile = GetTile(randomAnimal);
            //    List<Animal> aliveTileAnimals = aliveAnimals.Where(a => a.GetTile(map) == currentTile).ToList();
            //    List<IEatable> tileFoods = currentTile.Biome.Foods;
            //    string feedMsg = FeedAnimals(aliveAnimals);
            //    writer.WriteLine(feedMsg);
            //    RegenerateFood(foods);
            //    string moveMessage = randomAnimal.Move(sb);
            //    writer.WriteLine(moveMessage);
            //    //string output = GetStatistics(aliveAnimals);
            //    string output = GetSummary(aliveAnimals, 1);
            //    writer.WriteLine(output);
            //    writer.WriteLine("");
            //}

            if (getDetailedStatistics)
            {
                while (aliveAnimals.Any())
                {
                    Animal randomAnimal = aliveAnimals.ElementAt(random.Next(aliveAnimals.Count));
                    Tile currentTile = GetTile(randomAnimal);
                    List<Animal> aliveTileAnimals = aliveAnimals.Where(a => a.GetTile(map) == currentTile).ToList();
                    List<IEatable> tileFoods = currentTile.Biome.Foods;
                    string feedMsg = FeedAnimals(aliveAnimals);
                    writer.WriteLine(feedMsg);
                    RegenerateFood(foods);
                    string moveMessage = randomAnimal.Move(sb);
                    writer.WriteLine(moveMessage);
                    string output = GetStatistics(aliveAnimals);
                    writer.WriteLine(output);
                    writer.WriteLine("");
                }
            }
            else
            {
                for (int i = 1; i <= turns; i++)
                {
                    Animal randomAnimal = aliveAnimals.ElementAt(random.Next(aliveAnimals.Count));
                    Tile currentTile = GetTile(randomAnimal);
                    List<Animal> aliveTileAnimals = aliveAnimals.Where(a => a.GetTile(map) == currentTile).ToList();
                    List<IEatable> tileFoods = currentTile.Biome.Foods;
                    FeedAnimals(aliveAnimals);
                    RegenerateFood(foods);
                    string moveMessage = randomAnimal.Move(sb);
                    writer.WriteLine(moveMessage);
                    string output = GetSummary(aliveAnimals, i);
                    writer.WriteLine(output);
                    writer.WriteLine("");
                }
            }
        }

        private string PrintStartingMap(StringBuilder sb)
        {
            Tile[,] matrix = map.GetTiles();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sb.Append($"{matrix[i, j].Biome.Type}(capacity {matrix[i, j].Biome.Capacity})" + " ");
                }

                sb.AppendLine();
            }

            return sb.ToString().TrimEnd();
        }

        private Tile GetTile(Animal animal)
        {
            var tiles = map.GetTiles();
            Tile animalTile = default;

            foreach (var tile in tiles)
            {
                if (tile.Biome.Animals.Any(a => a.Name == animal.Name && a.MaxEnergy == animal.MaxEnergy))
                {
                    animalTile = tile;
                }
            }
            return animalTile;
        }

        private static string GetSummary(List<Animal> allAnimals, int turnCount)
        {
            StringBuilder sb = new StringBuilder();

            IEnumerable<Animal> deadAnimals = allAnimals.Where(a => a.IsAlive == false);
            IEnumerable<Animal> aliveAnimals = allAnimals.Where(a => a.IsAlive);

            sb.AppendLine($"Summary on turn {turnCount} :");
            sb.AppendLine($"Dead animals count: {deadAnimals.Count()}");
            sb.AppendLine($"Alive animals count: {aliveAnimals.Count()}");

            if (aliveAnimals.Any())
            {
                sb.AppendLine("Alive animals :");

                foreach (var animal in aliveAnimals)
                {
                    sb.AppendLine($"--{animal.ToString()}");
                }
            }

            return sb.ToString().TrimEnd();
        }

        private static string GetStatistics(List<Animal> allAnimals)
        {
            List<int> lifeSpans = new List<int>();
            StringBuilder sb = new StringBuilder();

            foreach (Animal animal in allAnimals)
            {
                lifeSpans.Add(animal.LifeSpan);
                sb.AppendLine(animal.ToString());
            }

            sb.AppendLine($"Min lifespan : {lifeSpans.Min()}");
            sb.AppendLine($"Max lifespan : {lifeSpans.Max()}");
            sb.AppendLine($"Average lifespan : {lifeSpans.Average():f2}");

            return sb.ToString().TrimEnd();
        }

        private IEatable GetRandomFood()
        {
            Random random = new Random();
            IEatable randomFood = allFoods.ElementAt(random.Next(allFoods.Count));

            return randomFood;
        }

        private string FeedAnimals(List<Animal> allAnimals)
        {
            StringBuilder sb = new StringBuilder();
            IEatable food = GetRandomFood();
            sb.AppendLine($"Food: {food.Name}");

            foreach (var animal in allAnimals.Where(a => a.IsAlive))
            {
                sb.AppendLine(animal.Feed(food));
            }

            return sb.ToString().TrimEnd();
        }

        private static void RegenerateFood(List<IEatable> allFoods)
        {
            foreach (var food in allFoods.OfType<Food>().Where(f => f.Nutrition < f.MaxNutrition))
            {
                food.Nutrition++;
            }
        }
    }
}
