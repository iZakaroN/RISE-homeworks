﻿namespace P07.CustomHashFunction
{
    public class Program
    {
        static void Main(string[] args)
        {
            Person person1 = new Person("Ivan", "Ivanov", 22);
            Person person2 = new Person("Petar", "Petrov", 70);
            Person person3 = new Person("Ivan", "Ivanov", 15);

            Console.WriteLine(person1.GetHashCode()); // 16016
            Console.WriteLine(person2.GetHashCode()); // 399000
            Console.WriteLine(person3.GetHashCode()); // 5985

            Console.WriteLine(person1.FirstName.Equals("Ivan")); // True
            Console.WriteLine(person1.Equals(new Person("Ivan", "Ivanov", 22))); // True
            Console.WriteLine(person1.Equals(new Person("Ivan", "Ivanov", 15))); // False
        }
    }
}