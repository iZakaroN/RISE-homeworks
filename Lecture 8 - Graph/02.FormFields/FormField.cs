﻿namespace _02.FormFields
{
    public class FormField
    {
        public static bool CalculateField(int[] allFields, int[] givenFields, int[,] nodeEdges)
        {
            int maxValue = allFields.Length;
            Graph graf = new Graph(maxValue);

            for (int i = 0; i < nodeEdges.GetLength(0); i++)
            {
                nodeEdges[i, 0] = maxValue + i;
            }

            for (int i = 0; i < nodeEdges.GetLength(0); i++)
            {
                for (int j = 1; j < nodeEdges.GetLength(1); j++)
                {
                    graf.AddEdge(nodeEdges[i, 0], nodeEdges[i, j]);
                }
            }

            if (graf.DetectCycle())
            {
                return false;
            }

            List<int> leftNodes = new List<int>();
            List<int> rightNodes = new List<int>();

            for (int i = 0; i < nodeEdges.GetLength(0); i++)
            {
                leftNodes.Add(nodeEdges[i, 0]);
            }

            for (int i = 0; i < nodeEdges.GetLength(0); i++)
            {
                for (int j = 1; j < nodeEdges.GetLength(1); j++)
                {
                    rightNodes.Add(nodeEdges[i, j]);
                }
            }

            var sectionR = rightNodes.Except(leftNodes).ToArray();
            var sectionL = leftNodes.Except(rightNodes).ToArray();

            if (graf.DetectCycle() == false && sectionR.SequenceEqual(givenFields) == true && sectionL.Any() == false)
            {
                return false;
            }

            return true;
        }
    }
}
