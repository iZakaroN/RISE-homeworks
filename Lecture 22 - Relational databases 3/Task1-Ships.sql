﻿ --1.Напишете заявка, която извежда броя на класовете бойни кораби.

 SELECT COUNT(TYPE) AS NO_Classes
 FROM CLASSES
 GROUP BY TYPE
 HAVING TYPE='bb';

 --2. Напишете заявка, която извежда средния брой оръдия за всеки клас боен кораб.

 SELECT CLASS, AVG(NUMGUNS) AS avgGuns
 FROM CLASSES
 WHERE TYPE='bb'
 GROUP BY CLASS;

 --3. Напишете заявка, която извежда средния брой оръдия за всички бойни кораби

 SELECT AVG(NUMGUNS) AS avgGuns
 FROM CLASSES
 WHERE TYPE='bb'
 GROUP BY TYPE;

-- 4. Напишете заявка, която извежда за всеки клас първата и последната година, в
--която кораб от съответния клас е пуснат на вода

SELECT CLASS, MIN(LAUNCHED) AS FirstYear, MAX(LAUNCHED) AS LastYear
FROM SHIPS
GROUP BY CLASS;

--5. Напишете заявка, която извежда броя на корабите, потънали в битка според класа

SELECT s.CLASS, COUNT(s.NAME) AS No_Sunk
FROM SHIPS AS s
JOIN OUTCOMES AS o ON s.NAME=o.SHIP
WHERE o.RESULT='sunk'
GROUP BY s.CLASS;

--6. Напишете заявка, която извежда броя на корабите, потънали в битка според
--класа, за тези класове с повече от 2 кораба.

SELECT s.CLASS, COUNT(s.NAME) AS No_Sunk
FROM SHIPS AS s 
JOIN OUTCOMES AS o ON s.NAME=o.SHIP
WHERE o.RESULT='sunk' AND s.CLASS IN (SELECT CLASS
										FROM SHIPS
										GROUP BY CLASS
										HAVING COUNT(NAME) > 2)
GROUP BY s.CLASS;


--7. Напишете заявка, която извежда средния калибър на оръдията на корабите за всяка страна???

SELECT COUNTRY, AVG(BORE) AS avg_bore
FROM CLASSES
GROUP BY COUNTRY;

