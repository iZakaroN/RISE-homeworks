﻿namespace P01.OrderBinaryTree
{
    public class Program
    {
        static void Main(string[] args)
        {
            BinaryTree node35 = new BinaryTree(35, null, null);
            BinaryTree node61 = new BinaryTree(61, null, null);
            BinaryTree node76 = new BinaryTree(76, null, null);

            BinaryTree node24 = new BinaryTree(24, null, null);
            BinaryTree node38 = new BinaryTree(38, node35, null);
            BinaryTree node69 = new BinaryTree(69, node61, node76);

            BinaryTree node11 = new BinaryTree(11, null, node24);
            BinaryTree node52 = new BinaryTree(52, node38, node69);
            BinaryTree node25 = new BinaryTree(25, node11, node52);

            BinaryTree binaryTree = new BinaryTree(25, node11, node52);
            List<BinaryTree> result = new List<BinaryTree>();

            //List<BinaryTree> output = binaryTree.DfsPreOrder(node25);
            //List<BinaryTree> output = binaryTree.DfsPostOrder(node25);
            List<BinaryTree> output = binaryTree.DfsInOrder(node25);

            foreach (var item in output)
            {
                Console.WriteLine(item.Value);
            }
        }
    }
}