﻿--Добавете нова колона num_pass към таблицата Flights, която ще съдържа броя на
--пътниците, потвърдили резервация за съответния полет.
--Добавете нова колона num_book към таблицата Agencies, която ще съдържа броя на
--резервациите към съответната агенция.

--ALTER TABLE FLIGHTS
--ADD num_pass INT NOT NULL DEFAULT 0;

--ALTER TABLE AGENCIES
--ADD num_book INT NOT NULL DEFAULT 0;

--Създайте тригер за таблицата Bookings, който да се задейства при вмъкване на
--резервация в таблицата и да увеличава с единица броя на пътниците, потвърдили
--резервация за таблицата Flights, както и броя на резервациите към съответната агенция.

--CREATE OR ALTER TRIGGER trgBookingInsert
--ON BOOKINGS
--FOR INSERT	
--AS
--BEGIN
--DECLARE @flight_id CHAR(10) = (SELECT FLIGHT_NUMBER FROM inserted);
--DECLARE @agency_name VARCHAR = (SELECT AGENCY FROM inserted);
--DECLARE @booking_status INT = (SELECT STATUS FROM inserted);

--    UPDATE FLIGHTS
--	SET num_pass = num_pass + 1
--	WHERE FNUMBER= @flight_id AND @booking_status=1;

--	UPDATE AGENCIES
--	SET num_book = num_book + 1
--	WHERE NAME = @agency_name AND @booking_status=1;
--END

--Създайте тригер за таблицата Bookings, който да се задейства при изтриване на
--резервация в таблицата и да намалява с единица броя на пътниците, потвърдили
--резервация за таблицата Flights, както и броя на резервациите към съответната агенция

--CREATE OR ALTER TRIGGER trgBookingDelete
--ON BOOKINGS
--FOR DELETE	
--AS
--BEGIN
--DECLARE @flight_id CHAR(10) = (SELECT FLIGHT_NUMBER FROM deleted);
--DECLARE @agency_name VARCHAR = (SELECT AGENCY FROM deleted);
--DECLARE @booking_status INT = (SELECT STATUS FROM deleted);

--    UPDATE FLIGHTS
--	SET num_pass = num_pass - 1
--	WHERE FNUMBER= @flight_id AND @booking_status=1;

--	UPDATE AGENCIES
--	SET num_book = num_book - 1
--	WHERE NAME = @agency_name AND @booking_status=1;
--END

--Създайте тригер за таблицата Bookings, който да се задейства при обновяване на
--резервация в таблицата и да увеличава или намалява с единица броя на пътниците,
--потвърдили резервация за таблицата Flights при промяна на статуса на резервацията.

CREATE OR ALTER TRIGGER trgBookingUpdate
ON BOOKINGS
FOR UPDATE	
AS
BEGIN
DECLARE @flight_id CHAR(10) = (SELECT FLIGHT_NUMBER FROM inserted);
DECLARE @agency_name VARCHAR = (SELECT AGENCY FROM inserted);
DECLARE @booking_status INT = (SELECT STATUS FROM inserted);

    UPDATE FLIGHTS
	SET num_pass = num_pass + 1
	WHERE FNUMBER= @flight_id AND @booking_status=1;

	UPDATE AGENCIES
	SET num_book = num_book + 1
	WHERE NAME = @agency_name AND @booking_status=1;

	UPDATE FLIGHTS
	SET num_pass = num_pass - 1
	WHERE FNUMBER= @flight_id AND @booking_status=0;

	UPDATE AGENCIES
	SET num_book = num_book - 1
	WHERE NAME = @agency_name AND @booking_status=0;
END