﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace L03_Team
{
    public class MatrixClass
    {
        static void Main(string[] args)
        {
            int[,] a = { { 2, 3, 5 }, { 55, 2, 8 }, { 34, 21, 13 } };
            FibonacciMatrix(a);
            FibonacciNeighbours(a);

            int[,] b = { { 1, 1, 1, 1, 1 }, { 2, 3, 5, 1, 1 }, { 55, 100, 8, 1, 1 }, { 34, 21, 13, 1, 1 }, { 1, 1, 1, 1, 1 } };
            FibonacciNeighbours(b);

            int[] c = { 1, 13, 8 };
            SortDictionaryByOrder(c);
        }

        public static string SortDictionaryByOrder(int[] input)
        {
            var binaryDict = new Dictionary<int, string>();
            var sumDict = new Dictionary<int, int>();
            var resultList = new List<int>();

            if (input.Length <= 1)
            {
                throw new Exception("The array must have at least 2 numbers.");
            }
            for (int i = 0; i <= input.Length - 1; i++)
            {
                string binaryNumber = ConvertToBinary(input[i]);
                binaryDict.Add(i, binaryNumber);
                sumDict.Add(i, FindSumBinary(binaryNumber));
            }

            var sortedSumsKeys = sumDict.OrderBy(x => x.Value)
                                        .ToDictionary(x => x.Key, x => x.Value);

            foreach (int i in sortedSumsKeys.Keys)
            {
                resultList.Add(input[i]);
            }

            string result = string.Join(" ", resultList.ToArray());
            Console.WriteLine(result);
            return result;

        }


        public static string ConvertToBinary(int number)
        {
            string binary = Convert.ToString(number, 2);
            return binary;
        }

        public static int FindSumBinary(string input)
        {
            int num = Convert.ToInt32(input);
            int sum = 0;

            while (num != 0)
            {
                sum += num % 10;
                num /= 10;
            }
            return sum;
        }

        public static bool FibonacciMatrix(int[,] matrix)
        {
            int n = matrix.GetLength(0);
            int[] fibonacciNumbers = GenerateFibonacci();
            bool result = false;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int col = 0; col < matrix.GetLength(0); col++)
                {
                    result = SquareCheck(matrix, fibonacciNumbers, n);

                    if (result == true)
                    {
                        Console.WriteLine("It's a fibonacci!");
                        return true;
                    }
                }

                if (result == true)
                {
                    break;
                }
            }

            if (result == false)
            {
                Console.WriteLine("Not a fibonacci!");
                return false;
            }

            return true;
        }

        public static bool FibonacciNeighbours(int[,] matrix)
        {
            int n = matrix.GetLength(0);
            int[] fibonacciNumbers = GenerateFibonacci();
            bool result = false;

            for (int i = 1; i < n; i++)
            {
                for (int j = 1; j < n; j++)
                {
                    if (IsInMatrix(i - 1, j - 1, n)
                        && IsInMatrix(i - 1, j, n)
                        && IsInMatrix(i - 1, j + 1, n)
                        && IsInMatrix(i, j + 1, n)
                        && IsInMatrix(i + 1, j + 1, n)
                        && IsInMatrix(i + 1, j, n)
                        && IsInMatrix(i + 1, j - 1, n)
                        && IsInMatrix(i, j - 1, n))
                    {
                        result = true;
                        List<int> neighbours = new List<int>();
                        neighbours.Add(matrix[i - 1, j - 1]);
                        neighbours.Add(matrix[i - 1, j]);
                        neighbours.Add(matrix[i - 1, j + 1]);
                        neighbours.Add(matrix[i, j + 1]);
                        neighbours.Add(matrix[i + 1, j + 1]);
                        neighbours.Add(matrix[i + 1, j]);
                        neighbours.Add(matrix[i + 1, j - 1]);
                        neighbours.Add(matrix[i, j - 1]);

                        int firstNumber = neighbours[0];
                        int firstNumberIndex = Array.IndexOf(fibonacciNumbers, firstNumber);

                        if (fibonacciNumbers.Contains(firstNumber))
                        {
                            for (int k = 1; k < neighbours.Count; k++)
                            {
                                if (fibonacciNumbers[firstNumberIndex + k] != neighbours[k])
                                {
                                    result = false;
                                    break;
                                }
                            }

                            if (result == true)
                            {
                                Console.WriteLine("There are fibonacci neighbours!");
                                return true;
                            }
                        }
                    }
                }
            }
            if (result == false)
            {
                Console.WriteLine("There aren't fibonacci neighbours!");
                return false;
            }

            return true;
        }

        private static bool IsInMatrix(int row, int col, int length)
        {
            return row >= 0 && row < length && col >= 0 && col < length;
        }

        private static bool SquareCheck(int[,] matrix, int[] fibonacciNumbers, int n)
        {
            bool isFibonacci = false;

            int dimention = n;
            for (int d = dimention; d >= 2; d--)
            {
                isFibonacci = true;
                List<int> border = new List<int>();

                for (int col = 0; col <= d - 1; col++)
                {
                    border.Add(matrix[0, col]);
                }

                for (int row = 1; row <= d - 2; row++)
                {
                    border.Add(matrix[row, d - 1]);
                }

                for (int col = d - 1; col >= 0; col--)
                {
                    border.Add(matrix[d - 1, col]);
                }

                for (int row = d - 2; row >= 1; row--)
                {
                    border.Add(matrix[row, 0]);
                }

                int firstNumber = border[0];
                int firstNumberIndex = Array.IndexOf(fibonacciNumbers, firstNumber);

                if (fibonacciNumbers.Contains(firstNumber))
                {
                    for (int i = 1; i < border.Count; i++)
                    {
                        if (fibonacciNumbers[firstNumberIndex + i] != border[i])
                        {
                            isFibonacci = false;
                            break;
                        }
                    }

                    if (isFibonacci)
                    {
                        return isFibonacci;
                    }
                }
            }

            return isFibonacci;
        }

        private static int[] GenerateFibonacci()
        {
            const int MaxFibonacciCount = 47;
            int[] fibonacciNumbers = new int[MaxFibonacciCount];
            fibonacciNumbers[0] = 0;
            fibonacciNumbers[1] = 1;
            fibonacciNumbers[2] = 1;

            for (int i = 3; i < MaxFibonacciCount; i++)
            {
                fibonacciNumbers[i] = fibonacciNumbers[i - 1] + fibonacciNumbers[i - 2];
            }

            return fibonacciNumbers;
        }
    }
}