﻿namespace P03.Coins
{
    public class Coin
    {
        private int vertexCount;
        List<List<int>> matrix;
        public Coin(int count)
        {
            vertexCount = count;
            matrix = new List<List<int>>();
            for (int i = 0; i < vertexCount; i++)
            {
                matrix.Add(new List<int>());
                for (int j = 0; j < vertexCount; j++)
                {
                    matrix[i].Add(0);
                }
            }
        }
        public void AddEdge(int start, int end)
        {
            matrix[start][end] = 1;
            matrix[end][start] = 1;
        }

        public bool CollectCoins(int node, int[,] nodeEdges)
        {
            int edgesCount = nodeEdges.GetLength(0);

            for (int i = 0; i < edgesCount; i++)
            {
                int startIndex = nodeEdges[i, 0];
                int endIndex = nodeEdges[i, 1];

                AddEdge(startIndex, endIndex);
                AddEdge(endIndex, startIndex);
            }

            HashSet<int> visited = new HashSet<int>();

            return AreAllVisited(node, visited);
        }

        private bool AreAllVisited(int node, HashSet<int> visited)
        {
            visited.Add(node);

            for (int i = 0; i < vertexCount; i++)
            {
                if (matrix[node][i] == 1 && !visited.Contains(i))
                {
                    AreAllVisited(i, visited);
                }
            }

            return visited.Count == matrix[node].Count ? true : false;
        }
    }
}
