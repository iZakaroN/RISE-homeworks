﻿namespace P01.CarNumbers
{
    public class CarNumbers
    {
        public static string GetCarOwner(Dictionary<string, string> carNumberOwner, string carNumber)
        {
            if (!carNumberOwner.ContainsKey(carNumber))
            {
                throw new KeyNotFoundException("No such car number!");
            }

            return carNumberOwner[carNumber];
        }

        public static string GetOwnersWithMoreCars(Dictionary<string, string> carNumberOwner)
        {
            Dictionary<string, int> ownersCountCars = new Dictionary<string, int>();

            foreach (var item in carNumberOwner)
            {
                if (!ownersCountCars.ContainsKey(item.Value))
                {
                    ownersCountCars[item.Value] = 1;
                }
                else
                {
                    ownersCountCars[item.Value]++;
                }
            }

            List<string> ownersWithCars = ownersCountCars.Where(c => c.Value > 1).Select(x => x.Key).ToList();

            return ownersWithCars.Count == 0
                ? "No owners with more than 1 car."
                : $"Owners with more than 1 car: {string.Join(", ", ownersWithCars)}";
        }
    }
}
